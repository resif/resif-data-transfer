[[_TOC_]]

# Guide de déploiement et d'utilisation du logiciel de transfert de données et métadonnées validées

## Introduction

Ce document s'adresse aux opérateurs des noeuds de collecte et
validation (« noeuds A ») du système d'information RESIF. Il décrit
l'installation, la configuration, et l'utilisation du logiciel de
transfert de données vers le noeud de distribution («* *noeud B ») du
système d'information RESIF.

Le logiciel `ResifDataTransfer` est mis à disposition des noeuds de
collecte et validation RESIF afin de transmettre leurs données validées
et de demander leur intégration dans le système d'information RESIF.
Ce processus comprend plusieurs étapes:

-   le transfert des données d'un **noeud A** vers le **noeud B**

-   la vérification des données  reçues par le **noeud B**
-   l'intégration de données dans les différentes bases de données du
    **noeud B**
-   la mise à disposition auprès du **noeud A** d'un journal
    d'intégration des données

## Documents de référence

« DCTP Transmission de données validées des noeuds A vers le noeud B »

« DCTP Types de données validées et conditions de leur intégration dans
le noeud B »

Note : la langue des documents de référence est le Français. Lorsque
jugé utile, la langue utilisée dans les aides en ligne des logiciels est
l'Anglais, afin de permettre l'utilisation quotidienne par des
opérateurs non francophones.

## Guide d'installation, mise à jour, et configuration

Se reporter aux instructions contenues dans les fichiers **INSTALL** et
**UPGRADE**, ainsi que dans le modèle de fichier de configuration
(`ResifDataTransfer.conf.dist`).

Cette documentation couvre les versions 2014.43 (et au delà) du
logiciel. Les versions antérieures ne doivent pas être exploitées en
production par les opérateurs.

## Guide d'exploitation
### Rappels

Cette section reprend les principaux éléments des documents de
références (voir page 3).

Le principe général de la transmission de données est une transmission
par lots (ou **transaction**). Une transaction est constituée :

-   d'un ensemble de fichiers transmis par un nœud de collecte et
    validation (un seul dans le cas d'un transfert de métadonnées
    sismologiques validées)
-   d'un type de données associé à cet ensemble de fichiers (par
    exemple : **données sismologiques validées au format miniseed**)
-   d'un état détaillé de la transaction. Cet état détaille le statut
    d'intégration des fichiers transmis au centre de distribution,
    selon les critères qualitatifs définis dans les documents de
    référence. Cet état prend la forme d'un fichier au format **XML**.
-   la structure du fichier associé à une transaction est décrite en
    Annexes.

### Fonctionnement général du logiciel, aide en ligne

Le logiciel est constitué :

-   d'un fichier exécutable **ResifDataTransfer**
-   d'un fichier de configuration textuel nommé librement, **ex :
    `ResifDataTransfer.conf`, **modifiable par l'opérateur. Un
    fichier de configuration générique est fourni avec l'application,
    accompagné de commentaires explicatifs.

L'exécution du logiciel s'effectue en ligne de commande. Il est ainsi
possible d'automatiser une partie des tâches de transfert (ex :
**cron**).

``` shell
$ ResifDataTransfer.py --help
```

Les sections suivantes présentent des cas d'utilisation courant du
logiciel. Dans la plupart des exemples présentés, les options peuvent
être exprimées sous forme longue (ex : **\--option**) ou courte (ex :
**-o**).

### Transmettre des données sismologiques validées

Désigner au logiciel de transfert un répertoire contenant les fichiers à
transmettre, dans cet exemple : `/mnt/mydata/`

Effectuer un test de transfert (ceci effectuera un test sans réaliser le
transfert effectif) :

```
$ ResifDataTransfer.py \\
  --send /mnt/mydata \\
  --data-type seismic_data_miniseed --test
```

Effectuer le transfert :

```
$ ResifDataTransfer.py --send /mnt/mydata --data-type seismic_data_miniseed
```

Spécifier un fichier de configuration alternatif :

```
$ ResifDataTransfer.py --config /etc/ResifDataTransfer.conf --send /mnt/mydata --data-type seismic_data_miniseed
```

Note : lorsque le transfert **rsync** est terminé, l'opérateur obtient
immédiatement un identifiant de transaction (voir 4.5).

### Transmettre des métadonnées sismologiques validées

Les commandes à executer et options disponibles sont les mêmes que
celles exposées en section 4.3. Seul l'option \--data-type doit être
altérée. Exemple:

```
$ ResifDataTransfer.py --send /mnt/my_metadata --data-type metadata_stationxml
```
### Obtenir un identifiant de transaction, lire les sorties standards et le journal de transaction

Lors de chaque transfert de données réussi (c'est à dire : dès que le
transfert **rsync** a réussi), le logiciel renvoi sur sa sortie
standard (**stdout**) un identifiant de transaction. Le format de cet
identifiant est décrit en Annexes.

Le logiciel émet un code de retour égal à 0 si le transfert s'est bien
déroulé. Des messages d'erreurs sont parfois émis sur la sortie
d'erreur standard (**stderr**).

Si le code retour est égal à 0 :

-   collecter sur **stdout** l'identifiant de transaction retourné
-   collecter **a posteriori** l'état de transaction (voir section
    4.6) puis effectuer les actions correctives si nécessaires
    (exemple : ré-envoi de données initialement rejetées).

Si celui-ci n'est pas égal à 0 :

-   collecter (ou ignorer) les messages émis sur **stderr**
-   vérifier manuellement le contenu des traces (voir l'option **log
    file** du fichier de configuration

Le logiciel renseigne un journal des transactions réussies (voir option
**logbook** du fichier de configuration), qu'il est possible
d'afficher de la façon suivante :

```
$ ResifDataTransfer.py --logbook --config /etc/ResifDataTransfert.conf
```

Le format de sortie du journal est détaillé en annexes.

Note : il existe un rare cas d'usage où un identifiant de transaction
ne sera pas écrit dans le **logbook**. Ce cas peut advenir en cas de
lancement parallèle (concurrent) de plusieurs instances de
**ResifDataTransfer.py** partageant le même fichier **logbook.** Ce
défaut n'a aucun impact sur la réception et le traitement des données
envoyées. Ce défaut devrait être traité lors d'une prochaine mise à
jour. Comme indiqué en **Annexes**, il est déconseillé de **parser** le
fichier **logbook** pour vos propres processus de traitement, le contenu
de ce fichier n'étant qu'informatif. Comme indiqué ci-dessus, il vous
incombe de collecter vous-même l'identifiant de transaction.

### Obtenir et analyser un état de transaction

Soit la transaction identifiée de façon unique par la chaîne AAA111.
Pour afficher sur la sortie standard (**stdout**) l'état de la
transaction :

```
$ ResifDataTransfer.py --retrieve-logs AAA111
```

Pour collecter dans un fichier local l'état de transaction :

```
$ ResifDataTransfer.py --retrieve-logs AAA111 > AAA111.xml
```

Un code d'erreur 0 est renvoyé si l'état de transaction a été obtenu,
1 sinon.

Notes:

-   Un descriptif du fichier XML est présent en **Annexes**
-   Un état de transaction évolue dans le temps : le fichier XML est
    complété « par morceaux » (voir Annexes) au fur et à mesure du
    traitement des données soumises. Il convient donc de collecter cet
    état de transaction tant qu'il n'a pas atteint son stade final.
-   Les états de transaction sont conservés sans limite de temps par le
    centre de distribution.
-   Une limitation technique connue a pour conséquence que, dans
    certains cas de figure, un état de transaction ne soit pas
    immédiatement disponible dès la soumission des données. Le symptome
    est l'affichage du message d'erreur \"Could not retrieve XML file
    for this transaction\", avec un code d'erreur en retour. Il
    convient de réitérer l'opération de récupération de l'état XML à
    intervals réguliers jusqu'à ce que le code retour redevienne 0.

Il est possible d'effectuer un affichage structuré ou d'explorer
l'état XML en utilisant des outils tiers en mode console (par exemple
**xmllint** ou **xmlstarlet**) ou les librairies XML de la plupart des
langages.

Exemples :

Afficher sous forme structuré le fichier XML:

``` shell
$ ResifDataTransfer.py --retrieve-logs AAA111 | xmllint --format -
```

ou

``` shell
$ ResifDataTransfer.py --retrieve-logs AAA111 | xmlstarlet format -
```

Obtenir le n° de transaction contenu dans un état XML :

``` shell
cat AAA111.xml | xmlstarlet select -t -m "transaction" -v @id -
```

Obtenir le statut courant d'une transaction :

``` shell
cat AAA111.xml | xmlstarlet select -t -m "transaction" -v @status -
```
Obtenir le type de données d'une transaction :

``` shell
cat AAA111.xml | xmlstarlet select -t -m "transaction" -v @datatype -
```

Obtenir la liste des fichiers envoyés lors d'une transaction :

```
cat AAA111.xml | xmlstarlet select -t -v "transaction/filelist/relativepath" -
```

Obtenir le code de retour du processus de traitement `T1` :

```
cat AAA111.xml | xmlstarlet select -t -c "transaction/process[@id='T1']" - | xmlstarlet select -t -m "process" -v @returncode -
```

Obtenir la liste des fichiers rejetés pour le processus de traitement `T8`:

```
cat AAA111.xml | xmlstarlet select -t -v "transaction/process[@id='T8']/rejectedfiles/relativepath" -
```

En complément des outils tiers pour lesquels des exemples sont fournis
ci-dessus, un utilitaire sur-mesure est fourni afin d**'aplatir** le
contenu du fichier XML. Cet outil lit sur son entrée standard
(**stdin**) un fichier XML d'état de transaction. Il est possible de le
chaîner avec le logiciel de transfert, soit par un **pipe **soit par un
fichier intérmédiaire. Cet outil est actuellement à l'état de prototype
et sujet à évolutions. Il ne fonctionne pas avec Python 2.6 (limitation
de la librairie XML).

Exemple :

```
$ ResifDataTransfer.py --retrieve-logs AAA111 > AAA111.xml
$ cat AAA111.xml | ResifDataTransferTransaction.py --print-transaction-id AAA111
```

L'aide en ligne recense les options disponibles :

```
$ ResifDataTransferTransaction.py --help

--print-transaction-id : affiche l'identifiant de transaction
--print-status : affiche le code d'état de transaction
--print-datatype : affiche le type de données
--print-filelist : affiche la liste de fichiers soumis initialement par le centre de collecte et de validation
--print-process-returncode processID : affiche le code de retour pour le traitement identifié par **processID
--print-process-rejectedfiles processID : affiche la liste des fichiers rejetés lors du traitement identifié par processID
```

Exemple :
```
$ cat AAA111.xml | ResifDataTransferTransaction.py --print-process-returncode T1
0

$ cat AAA111.xml | ResifDataTransferTransaction.py --print-process-returncode T8
1

$ cat AAA111.xml \| ResifDataTransferTransaction.py --print-process-rejectedfiles T8
2010/FR/STA1/HHE.D/FR.STA1.00.HHE.D.2010.065
2010/FR/STA2/HHZ.D/FR.STA2.00.HHZ.D.2010.073
2012/FR/STA2/HHZ.D/FR.STA2.00.HHZ.D.2012.100
```
# Annexes
## Format de l'identifiant de transaction


L'identifiant est une chaîne de caractères constituée de 1 à 16
caractères. Seuls les caractères** **[A-Za-z0-9]** sont utilisés. Cet
identifiant est unique, y compris entre transactions issues de
différents nœuds de collecte et validation.

## État d'intégration de données miniseed validées : structure du fichier XML

Ci-dessous un extrait commenté d'un fichier XML concernant l'état
d'une intégration de données.

``` xml
<?xml version="1.0" encoding="UTF-8"?>
<!--
  Les informations sur l’état de la transaction sont encapsulées dans la balise
  « transaction ». Cette balise porte des attributs qualifiant le type
  de données, son identifiant, le noeud de collecte d’origine, et le statut
  de la transaction.

  Signification des statuts :

    0 données transmises via rsync au centre de distribution
    4 données reçues au centre de distribution et en cours
      d’analyse par le système d’intégration
    8 données reçues au centre de distribution et analyse par le
      système d’intégration terminée
  128 erreur fatale durant l’analyse des données
-->
<transaction datatype="seismic_data_miniseed"
                   id="BRDP6909"
            resifnode="TEST"
               status="2">
<!-- Commentaire libre inséré par la chaine d’intégration -->
<comment>all file-checking steps done</comment>

<!--
  Date de création initiale de la transaction (format ISO8601).
  Cette date étant générée par le client, il est fortement conseillé
  que votre horloge système soit synchronisée par NTP.
-->
<datecreated>2013-08-28T09:45:00Z</datecreated>

<!--
  Date de dernière modification de l’état de transaction
  par la chaine d’intégration (format ISO8601)
-->
<lastupdated>2013-08-28T10:00:11Z</lastupdated>

<!--
  Volume total des fichiers transmis, mesuré par le client.
  Le volume est arrondi et exprimé selon l’unité symbolisée par l’attribut
  unit (b = octets, mb = megaoctets, gb = gigaoctets)
-->
<clientsize unit=‘gb’>18.78</clientsize>

<!--
  Liste des fichiers transmis. Chaque fichier est exprimé sous
  la forme d’un chemin relatif au répertoire source.
-->
<filelist>
  <relativepath>2010/FR/CHIF/HHE.D/FR.CHIF.00.HHE.D.2010.074</relativepath>
  <relativepath>2010/FR/CHIF/HHE.D/FR.CHIF.00.HHE.D.2010.065</relativepath>
  <relativepath>2010/FR/CHIF/HHE.D/FR.CHIF.00.HHE.D.2010.073</relativepath>
</filelist>

<!--
  Chaque fichier présent dans filelist (ci-dessus) est soumis à
  une série de tests (process) permettant de vérifier les
  conditions d’intégration (voir documents de référence).
  Un process est doté d’un identifiant symbolique (ici « T1 »),
  invariant et documenté ci-dessous.

  L’ordre d’éxecution est signifié sous l’attribut « rank ».

  Le code de retour est indiqué sous l’attribut « returncode ».
  Ces code ne présagent pas de la recevabilité des fichiers
  traités mais permettent de suivre le fonctionnement global
  de la chaîne d’intégration. Ainsi, même si 100% de fichiers miniseed* *
  sont considérés comme “non admissibles” par un process donné, il est
  tout à fait possible de trouver 0 en code de retour, dans le sens où le
  traitement s’est passé normalement et peut continuer vers le
  traitement suivant.
  0 : traitement terminé avec succès
  1 : traitement terminé avec warning (toutefois le traitement
  suivant peut être lancé)
  128 : erreur fatale (les traitements suivants sont abandonnés)
-->
<process id="T1" rank="1" returncode="0">

<!-- Commentaire libre inséré par la chaine d’intégration. -->
<comment>Lorem ipsum dolor sit amet, consectetur elit. </comment>

<!-- Liste des fichiers non admissibles -->
<rejectedfiles>
  <relativepath>2010/FR/CHIF/HHE.D/FR.CHIF.00.HHE.D.2010.074</relativepath>
  <relativepath>2010/FR/CHIF/HHE.D/FR.CHIF.00.HHE.D.2010.065</relativepath>
</rejectedfiles>
</process>
<!--
  Suivent ensuite tous les autres traitements successifs,
  décrits ci-dessous.
-->
<process id="T2" rank="2" returncode="0">
</process>
</transaction>
```

## Description des étapes d'intégration

### Intégration de donnée validée au format miniSEED

Les **process** dédiés à l'intégration des données validées sont les
suivants (voir document de référence) :

* **T1** : effectue un inventaire des fichiers transmis, et détermine
s'ils sont bien au format miniseed et contiennent bien des en-têtes
miniseed. Utilise la commande **msi** (IRIS, v3.4.3) avec les options
**-d** ou **-tg** ou **-p**. Tous les fichiers sont analysés.
* **T2** : vérifie la syntaxe des noms de fichiers, qui doit
respecter la norme SDS (NETWORK.STATION.LOCATION.CHANNEL.TYPE.YYYY.DDD)
(cf. document de référence, page 5). Tous les fichiers qui satisfont T1
sont analysés.
* **T3** : Vérifie le démultiplexage complet des données dans chaque
fichier (cf ref, condition A, page 8). L'unicité de fréquence
d'échantillonnage dans le fichier est également vérifiée.
Les noms des fichiers dans lesquels le démultiplexage n'est pas complet
ou qui contiennent des segments dont les fréquences d'échantillonnage
ne sont pas cohérentes avec le code du canal (selon le Manuel SEED) sont
pointés. Tous les fichiers qui satisfont le test T1 sont analysés, y
compris les fichiers qui ne satisfont pas T2.
* **T4** : Vérifie la taille des blocs, qui doit être égale à 4096
octets. Une taille de bloc de 512 est acceptée pour les canaux basse
fréquence correspondant aux lettres L,V,U,R,P,T,Q. 
Les noms des fichiers dont le contenu ne satisfait pas cette
condition sont pointés (cf ref, condition E, page 8). Tous les fichiers
qui satisfont T1 sont analysés, y compris les fichiers qui ne satisfont
pas les T2 et T3.
* **T5** : Vérifie le label de qualité de chaque bloc d'un fichier, qui
doit être 'D','M','Q'. Les noms des fichiers dont le contenu ne
satisfait pas cette condition sont pointés (cf ref, condition D, page
8). Tous les fichiers qui satisfont T1 sont analysés, y compris les
fichiers qui ne satisfont pas les T2, T3 et T4.
* **T6** : Vérifie la cohérence entre les quatre premiers champs du nom
du fichier et le **header** de chaque bloc. Les noms des fichiers dont
le contenu ne satisfait pas cette condition sont pointés (cf ref,
condition A, page 8 et page 5). Tous les fichiers qui satisfont T1 sont
analysés, y compris les fichiers qui ne satisfont pas les T3, T4, T5.
* **T7** : Vérifie la durée de la fenêtre dans le fichier correspond à la
durée désignée par le nom du fichier. Les noms des fichiers dont le
contenu ne satisfait pas cette condition sont pointés (cf ref, condition
A, page 8 et page 5). Tous les fichiers qui satisfont T1 sont analysés,
y compris les fichiers qui ne satisfont pas les T3, T4, T5, T6.
* **T8** : Vérifie l'encodage et la compression des données. Les noms
des fichiers dont le contenu ne satisfait pas cette condition sont
pointés (cf ref, condition C, page 8). Tous les fichiers qui satisfont
T1 sont analysés, y compris les fichiers qui ne satisfont pas les T3,
T4, T5, T6,T7
* **T9** : Vérifie que les métadonnées connues du centre de distribution
(« noeud B ») décrivent chacun des blocs d'un fichier (cf ref,
condition G, page 8). Les gaps dans les métadonnées dont la durée est
inférieure à 1/fréquence d'échantillonnage du record ne sont pas pris
en compte.
Cette condition n'est pas bloquante (les données qui ne satisfont pas
cette condition sont archivées par T10). Tous les fichiers qui satisfont
T1 sont analysés, y compris les fichiers qui ne satisfont pas les T3,
T4, T5, T6,T7 , T8.
* **T10** : Intègre les données. Seuls les fichiers qui satisfont les
tests T1 à T9 sont intégrés. Les répertoires de l'arborescence
correspondants aux fichiers soumis pour intégration sont crées et les
fichiers SDS sont copiés vers l'archive de données centralisée RESIF.
Les fichiers non intégrés sont pointés. Si il existe dans le zone
'BUD' du noeud B des fichiers bruts (provenant de la collecte en temps
réel des données) de noms analogues à ceux des fichiers validés
intégrés,  ces fichiers bruts sont compressés et archivés, il ne sont
plus disponibles pour la distribution.
* **T11** : Met à jour la base de données interne du centre de données,
afin que les données soient disponibles par le webservice
**fdsn-dataselect**. Les fichiers non inventoriés sont pointés. Seuls
les fichiers qui satisfont les tests T1 à T10 sont inventoriés.

## Intégration de données PH5 validée

Liste des process et leur signification :

* **T0** : Tous les fichiers `*.ph5` transmis sont des archives ph5 valides.
Le test est fait avec la commande UNIX `file`.
* **T0bis** : Toues les fichiers concernent le même réseau sismologique.
* **T1** : Le code réseau concerné est bien présent dans la base de donnée d'inventaire.
C'est à dire qu'une métadonnée concernant ce réseau a déjà été soumise au centre de données.
* **T2** : Toutes les stations sont présentes dans la base de donnée d'inventaire.
* **T3** : L'archive PH5 peut être convertie en miniseed valide. Pour cela, on extrait une heure au hasard et on teste le format obtenu avec la commande `msi`.
* **T4** : L'archive soumise est copiée par rsync sur le dépôt des données validées.
* **T5** : Les fichiers copiés sont inventoriés en base de donnée.

## Intégration de métadonnées validées

Les **process** dédiées à l'intégration de métadonnées validées sont les
suivants (voir document de réfèrence) :

* **T0** : vérifie qu'un unique fichier est transmis dans la
transaction en cours.
* **T1** : **première** validation du fichier au format
stationXML. L'outil de validation est `station-xml-validator-1.7.1.jar`
Ce test est non bloquant, toutes les règles du validator s'appliquent.
Une erreur sur ce test signale simplement au noeud A qu'il y a une violation
des régles 222 et 223 (https://github.com/iris-edu/stationxml-validator/wiki/StationXML-Validation-Rule-List
pour la définition de ces règles).
* **T2** : **deuxième** validation du fichier au format stationXML.
L'outil de validation est : `station-xml-validator-1.7.1.jar`.
Ce test est bloquant; les règles 222 et 223 sont contournées, afin de permettre
l'intégration de station dont les channels varient de plus de 1km du point de référence
de la station.
* **T3** : vérifie que le volume stationXML ne porte que sur un seul
réseau et une seule station.
* **T4** : vérifie que le réseau (avec les années de début et de fin)
référencé dans le volume stationXML est connu de la base de données
resifInv. Tout nouveau réseau (ou toute modification de date de
début/fin de réseau) doit être demandée à mailto:resif-dc@univ-grenoble-alpes.fr
* **T5** : vérifie que les métadonnées soumises n'ont pas déjà été définies sous un autre nom de fichier.
* **T6** : vérification propre au réseau FR, qui contient des
stations colocalisées : une station de FR est initialisée si et
seulement si le nom du fichier du volume stationXML a pour préfixe FR
* **T7** : génère le code SQL de mise à jour de la base de données resifInv
* **T8** : vérifie que la métadonnée ne rend pas obsolète les statistiques PPSD et si c'est le cas
marque le PPSD pour un recalcul ultérieur.
* **T9** : exécute le code de mise à jour de la base de données
resifInv. Différentes contraintes d'intégrité internes au noeud B sont
vérifiées à ce moment. En cas de code retour 128, il est demandé au
noeud A de prendre contact avec le noeud B.

## Format du journal de transaction

Ce fichier est au format **JSON**. Le contenu et le format de ce fichier
étant susceptibles d'évoluer, il est recommandé de ne pas
l'interpréter directement. L'option `--logbook` permet
l'affichage du journal de transaction sous forme tabulaire. Ceci décrit
le format de cette sortie :

Les lignes commençant par le caractère `#` sont des commentaires
libres. Les autres lignes sont des valeurs séparées par des tabulations,
dans cet ordre :

1.  identifiant de transaction (**str**)
2.  date de transaction au format ISO8601 (**str**),
3.  nom du noeud RESIF transmetteur (**str**, voir fichier de
    configuration),
4.  type de données transmises (**str**)
5.  chemin complet du répertoire transmis (**str**),
6.  volume total des fichiers transmis en gigaoctets (**float**)

Un caractère `-` remplace une valeur vide ou non pertinente.

Les évolutions feront en sorte que les champs additionnels viennent à la
suite de ces champs (et non en insertion ou remplacement).

## Historique des modifications
### Mai 2020
Support du formta de données PH5

J. Schaeffer
### ​Octobre 2019 v2.0
La transmission des volumes SEED dataless pour les métadonnées n'est plus supportée
La transmission des volumes stationXML est supportée
Implementation d'une limite en terme de nombres de fichiers (à l'exécution)

J. Touvier
R. Bouazzouz
J. Schaeffer
C. Péquegnat
### Mai 2016
Modification de la description du test T10 pour l'intégration des données
Modification de la description du test T2 pour l'intégration des métadonnées
### v1.0.5 ​Avril 2016
​Ajout d'une note en section 4.5 concernant un bug peu fréquent en cas de lancement de plusieurs instances de `ResifDataTransferTransaction.py` en parallèle.
### v1.0.4 ​Octobre 2015 P.Volcke
Ajout de notes concernan t une limitation de `ResifDataTransferTransaction.py` et des solutions alternatives avec xmlstarlet, et une note concernant une limitation technique sur la disponibilité de l'état de transaction.

### v1.0.3 Août 2015	P.Volcke
​Ajout des élèments concernant la transmission de métadonnées.
### v1.0.2 Août 2014	P.Volcke
Précision sur la collecte du numéro de transaction par l'appelant (4.5).
### v1.0.1 24 Mars 2014, C.Pequegnat
Modification de description du processus T3 (intégration de données)
### v1.0 18 Mars 2014, P.Volcke C.Pequegnat
Mise en production du système
### v0.3 20 Déc 2013, P.Volcke C.Pequegnat
Modifications suites à suggestions des centres de collecte (ajouts d'informations liées à l'intégration des données sismologiques validées + précisions sur le workflow)
### v0.2 3 Oct 2013	, P.Volcke C.Pequegnat
Refonte du document (prototype fonctionnel du logiciel)
### v0.1 19 Juin 2012, P. Volcke
Écriture initiale initiale (maquette non fonctionnelle
